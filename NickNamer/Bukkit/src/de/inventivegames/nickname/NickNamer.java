/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.inventivegames.nickname.database.SkinEntry;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.inventivetalent.update.spigot.SpigotUpdater;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mcstats.MetricsLite;

import javax.persistence.PersistenceException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * © Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 */
public class NickNamer extends JavaPlugin implements Listener, PluginMessageListener {

	public static NickNamer instance;
	public static String prefix = "§6[NickNamer]§r ";

	private static PacketListener packetListener;

	public static long    API_TIMEOUT       = 10000;
	public static boolean NICK_TAB          = true;
	public static boolean NICK_CHAT         = true;
	public static boolean NICK_SCOREBOARD   = true;
	public static boolean NICK_NAME         = true;
	public static boolean NICK_VANILLA      = true;
	public static boolean NICK_COMMANDS     = true;
	public static boolean LOADING_SKIN      = true;
	public static boolean BUNGEECORD        = false;
	public static boolean API_ONLY          = false;
	public static boolean STORAGE_ENABLED   = false;
	public static long    STORAGE_TIME      = 10080L;
	public static boolean SELF_UPDATE       = true;
	public static boolean SINGLE_COMMAND    = false;
	public static boolean CHANGE_BOTH       = false;
	public static boolean TAB_OVERWRITE     = true;
	public static boolean TAB_KEEP_ORIGINAL = false;
	public static boolean CHAT_REPLACE      = true;
	public static boolean AUTO_NICK         = false;

	public static int     MAX_LENGTH              = 16;
	public static boolean MAX_LENGTH_IGNORE_COLOR = true;

	public static boolean QUICK_COMMAND    = true;
	public static boolean DISABLE_EXISTING = false;
	public static List<String> RANDOM_NAMES;
	public static List<String> RANDOM_SKINS;

	public static String MSG_PERM_GENERAL      = "§cNo permission.";
	public static String MSG_PERM_NAME         = "§cYou don't have permission to use that name!";
	public static String MSG_PERM_SKIN         = "§cYou don't have permission to use that skin!";
	public static String MSG_PERM_CHANGE_OTHER = "§cYou don't have permission to change other's nicknames.";
	public static String MSG_NAME_INVALID      = "§cInvalid nick name!";
	public static String MSG_NAME_LENGTH       = "§cThat name is too long!";
	public static String MSG_NAME_CHANGED      = "§aChanged §b%player%'s §aname to §b%name%§a.";
	public static String MSG_NAME_TAKEN        = "§cThis name is already taken!";
	public static String MSG_SKIN_CHANGED      = "§aChanged §b%player%'s §askin to §b%skin%'s§a skin.";
	public static String MSG_SKIN_UPDATE       = "§aIt will be updated in §b%time%§a minute(s).";
	public static String MSG_CLEARED_NAME      = "§aCleared §b%player%'s §aname.";
	public static String MSG_CLEARED_SKIN      = "§aCleared §b%player%'s §askin.";
	public static String MSG_PLAYER_NOT_ONLINE = "§cThat player is not online!";

	@Override
	public void onEnable() {
		instance = this;
		Bukkit.getPluginManager().registerEvents(this, this);

		this.saveDefaultConfig();
		prefix = this.getConfig().getString("message.prefix", prefix).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		API_TIMEOUT = this.getConfig().getLong("apiTimeout", API_TIMEOUT);
		NICK_TAB = this.getConfig().getBoolean("nick.tab", NICK_TAB);
		NICK_CHAT = this.getConfig().getBoolean("nick.chat", NICK_CHAT);
		NICK_SCOREBOARD = this.getConfig().getBoolean("nick.scoreboard", NICK_SCOREBOARD);
		NICK_NAME = getConfig().getBoolean("nick.name", NICK_NAME);
		NICK_VANILLA = getConfig().getBoolean("nick.vanilla", false);
		NICK_COMMANDS = getConfig().getBoolean("nick.commands", false);

		QUICK_COMMAND = this.getConfig().getBoolean("quickCommand", QUICK_COMMAND);
		DISABLE_EXISTING = this.getConfig().getBoolean("disableExisting", DISABLE_EXISTING);
		LOADING_SKIN = this.getConfig().getBoolean("loadingSkin", LOADING_SKIN);
		BUNGEECORD = this.getConfig().getBoolean("bungeecord", BUNGEECORD);
		API_ONLY = this.getConfig().getBoolean("apiOnly", API_ONLY);
		RANDOM_NAMES = new ArrayList<>();
		for (String s : this.getConfig().getStringList("randomNames")) {
			RANDOM_NAMES.add(s.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2"));
		}
		RANDOM_SKINS = new ArrayList<>();
		for (String s : getConfig().getStringList("randomSkins")) {
			RANDOM_SKINS.add(s.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2"));
		}
		STORAGE_ENABLED = getConfig().getBoolean("localStorage.enabled", false);
		STORAGE_TIME = getConfig().getLong("localStorage.cacheTime", STORAGE_TIME);
		SELF_UPDATE = getConfig().getBoolean("selfUpdate", true);
		SINGLE_COMMAND = getConfig().getBoolean("singleCommand", false);
		CHANGE_BOTH = getConfig().getBoolean("changeBoth", false);
		TAB_OVERWRITE = getConfig().getBoolean("tabOverwrite.enabled", true);
		TAB_KEEP_ORIGINAL = getConfig().getBoolean("tabOverwrite.keepOriginal", false);
		CHAT_REPLACE = getConfig().getBoolean("chatReplacement", true);
		AUTO_NICK = getConfig().getBoolean("autoNick", false);

		MAX_LENGTH = getConfig().getInt("maxLength.value", MAX_LENGTH);
		MAX_LENGTH_IGNORE_COLOR = getConfig().getBoolean("maxLength.ignoreColors", MAX_LENGTH_IGNORE_COLOR);

		MSG_PERM_GENERAL = this.getConfig().getString("message.permission.general", MSG_PERM_GENERAL).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PERM_NAME = this.getConfig().getString("message.permission.name", MSG_PERM_NAME).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PERM_SKIN = this.getConfig().getString("message.permission.skin", MSG_PERM_SKIN).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PERM_CHANGE_OTHER = this.getConfig().getString("message.permission.change_other", MSG_PERM_CHANGE_OTHER).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_INVALID = this.getConfig().getString("message.name.invalid", MSG_NAME_INVALID).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_LENGTH = this.getConfig().getString("message.name.length", MSG_NAME_LENGTH).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_CHANGED = this.getConfig().getString("message.name.changed", MSG_NAME_CHANGED).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_TAKEN = this.getConfig().getString("message.name.taken", MSG_NAME_TAKEN).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_SKIN_CHANGED = this.getConfig().getString("message.skin.changed", MSG_SKIN_CHANGED).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_SKIN_UPDATE = this.getConfig().getString("message.skin.update", MSG_SKIN_UPDATE).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_CLEARED_NAME = this.getConfig().getString("message.name.cleared", MSG_CLEARED_NAME).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_CLEARED_SKIN = this.getConfig().getString("message.skin.cleared", MSG_CLEARED_SKIN).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PLAYER_NOT_ONLINE = this.getConfig().getString("message.player.not_online", MSG_PLAYER_NOT_ONLINE).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");

		packetListener = new PacketListener(this);

		if (!API_ONLY) {
			CommandHandler handler = new CommandHandler();
			getCommand("nickname").setExecutor(handler);
			getCommand("skin").setExecutor(handler);

			if (TAB_OVERWRITE) {
				Bukkit.getPluginManager().registerEvents(new TabCompleter(), this);
			}
		} else {
			getLogger().info("Enabled 'api only' mode");
		}

		System.out.println("[NickNamer] Starting SkinLoader.");
		if (SkinLoader.instance != null) {
			SkinLoader.instance.active = false;
			SkinLoader.instance = null;
		}
		new SkinLoader().start();

		if (!Bukkit.getOnlineMode() && !BUNGEECORD) {
			System.out.println("[NickNamer] Server is in offline mode. Starting UUIDResolver.");
			if (UUIDResolver.instance != null) {
				UUIDResolver.instance.active = false;
				UUIDResolver.instance = null;
			}
			new UUIDResolver().start();
		}

		if (STORAGE_ENABLED) {
			long delay = STORAGE_TIME * 60 * 1000;
			int count = -1;
			try {
				count = getDatabase().find(SkinEntry.class).findRowCount();
			} catch (PersistenceException e) {
				getLogger().info("Installing database");
				installDDL();
			}
			if (count > 0) {
				getLogger().info("Loading stored skin data (" + count + " entries)");
				List<SkinEntry> list = getDatabase().find(SkinEntry.class).findList();
				for (SkinEntry entry : list) {
					long diff = System.currentTimeMillis() - entry.getLastUsage();
					if (delay > -1 && diff > delay) {
						getLogger().info("Ignoring entry for " + entry.getOwner() + ", last usage: " + diff + "ms");
						continue;
					} else {
						try {
							UUID id = UUID.fromString(entry.getOwner());
							JSONObject json = (JSONObject) (new JSONParser()).parse(entry.getData());
							SkinLoader.skinStorage.put(id, json);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "NickNamer");
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "NickNamer", this);

		try {
			MetricsLite metrics = new MetricsLite(this);
			if (metrics.start()) {
				System.out.println("[NickNamer] Metrics started.");
			}
		} catch (Exception e) {
		}

		try {
			SpigotUpdater updater = new SpigotUpdater(this, 5341);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Class<?>> getDatabaseClasses() {
		List<Class<?>> list = new ArrayList<>();
		list.add(SkinEntry.class);
		return list;
	}

	@Override
	public void onDisable() {
		if (packetListener != null) {
			packetListener.disable();
		}

		List<Player> online = new ArrayList<>(Bukkit.getOnlinePlayers());

		if (online.isEmpty()) {
			getLogger().warning("Could not send shutdown message: No players are online");
			return;
		}

		//Send the shutdown message
		String names = "";// "player=nick"
		String skins = "";// "player=skin"
		String data = "";// "owner=data"

		for (Player p : online) {
			if (Nicks.isNicked(p.getUniqueId())) {
				String nick = Nicks.getNick(p.getUniqueId());
				names += "+#+" + (p.getUniqueId().toString() + "=#=" + nick);
			}
			if (Nicks.hasSkin(p.getUniqueId())) {
				UUID skin = Nicks.getSkin(p.getUniqueId());
				skins += "+#+" + (p.getUniqueId().toString() + "=#=" + Bukkit.getOfflinePlayer(skin).getName());
			}
		}
		for (Map.Entry<UUID, JSONObject> entry : SkinLoader.skinStorage.entrySet()) {
			data += "+#+" + (entry.getKey().toString() + "=#=" + entry.getValue().toJSONString());
		}

		if (names.length() > 0) { names = names.substring(3); }
		if (skins.length() > 0) { skins = skins.substring(3); }
		if (data.length() > 0) { data = data.substring(3); }

		if (names.isEmpty() && skins.isEmpty() && data.isEmpty()) { return; }// Don't continue if there's nothing useful to be sent

		try {
			Field field = JavaPlugin.class.getDeclaredField("isEnabled");
			field.setAccessible(true);
			field.setBoolean(this, true);// Unfortunately, the plugin has to be enabled to send messages. Hopefully this breaks nothing else.

			sendPluginMessage(online.get(0), "shutdown", names, skins, data);

			field.setBoolean(this, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();

		if (Nicks.isNicked(p.getUniqueId())) {
			String nick = Nicks.getNick(p.getUniqueId());
			if (NickNamer.NICK_CHAT) {
				p.setDisplayName(nick);
			}
			if (NickNamer.NICK_TAB) {
				p.setPlayerListName(nick);
			}
		}

		//Auto-nick
		if (AUTO_NICK && p.hasPermission("nick.autonick")) {
			if (!Nicks.isNicked(p.getUniqueId())) {
				Bukkit.getScheduler().runTaskLater(this, new Runnable() {
					@Override
					public void run() {
						p.chat("/nick random");
					}
				}, 20);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onChat(AsyncPlayerChatEvent e) {
		if (!NickNamer.CHAT_REPLACE) { return; }
		if (e.isCancelled()) {return; }
		String message = e.getMessage();
		for (String nick : Nicks.getUsedNicks()) {
			List<UUID> users = Nicks.getPlayersWithNick(nick);
			for (UUID id : users) {
				OfflinePlayer target = Bukkit.getOfflinePlayer(id);
				if (message.contains(target.getName())) {
					message = message.replace(target.getName(), nick);
				}
			}
		}
		e.setMessage(message);
	}

	public static void sendPluginMessage(Player player, String action, String... values) {
		if (player == null || !player.isOnline()) { return; }

		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(action);
		out.writeUTF(player.getUniqueId().toString());
		for (String s : values) {
			out.writeUTF(s);
		}
		player.sendPluginMessage(instance, "NickNamer", out.toByteArray());
	}

	@Override
	public void onPluginMessageReceived(String s, Player player, byte[] bytes) {
		if ("NickNamer".equals(s)) {
			ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
			String sub = in.readUTF();
			UUID who = UUID.fromString(in.readUTF());
			if ("name".equals(sub)) {
				String name = in.readUTF();

				if (name == null || "reset".equals(name)) {
					Nicks.removeNick(who);
				} else {
					Nicks.setNick(who, name);
				}
			} else if ("skin".equals(sub)) {
				String skin = in.readUTF();

				if (skin == null || "reset".equals(skin)) {
					Nicks.removeSkin(who);
				} else {
					Nicks.setSkin(who, skin);
				}
			} else if ("data".equals(sub)) {
				try {
					UUID id = UUID.fromString(in.readUTF());
					JSONObject data = (JSONObject) (new JSONParser()).parse(in.readUTF());
					SkinLoader.skinStorage.put(id, data);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				getLogger().warning("Unknown incoming plugin message: " + sub);
			}
		}
	}
}
