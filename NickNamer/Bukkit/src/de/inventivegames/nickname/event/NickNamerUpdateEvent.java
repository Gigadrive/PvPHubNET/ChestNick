package de.inventivegames.nickname.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

/**
 * Event called when the name or skin of a player is updated to a observer <br/>
 * All changes made to the event only affect the current observer <br/>
 * If the name or skin is set to null, the original value will be used
 */
public class NickNamerUpdateEvent extends Event implements Cancellable {

	private final Player player;
	private final Player observer;
	private       String nick;
	private       UUID   skin;

	private boolean cancelled;

	public NickNamerUpdateEvent(Player who, Player observer, String nick, UUID skin) {
		super(true);

		this.player = who;
		this.observer = observer;
		this.nick = nick;
		this.skin = skin;
	}

	/**
	 * @return The {@link org.bukkit.entity.Player} which is being updated
	 */
	@Nonnull
	public final Player getPlayer() {
		return player;
	}

	/**
	 * @return The {@link org.bukkit.entity.Player} which sees the update
	 */
	@Nonnull
	public final Player getObserver() {
		return observer;
	}

	/**
	 * @return The nickname used by the player (or the real name, if no nickname is used)
	 */
	@Nullable
	public String getNick() {
		return nick;
	}

	/**
	 * Changes the nickname
	 *
	 * @param nick The new nickname
	 */
	public void setNick(@Nullable String nick) {
		if (nick != null && nick.length() > 16) {
			throw new IllegalArgumentException("The name cannot be longer than 16 characters");
		}
		this.nick = nick;
	}

	/**
	 * @return The {@link java.util.UUID} of the used Skin
	 */
	@Nullable
	public UUID getSkin() {
		return skin;
	}

	/**
	 * Changes the skin
	 *
	 * @param skin The {@link java.util.UUID} of the new skin
	 */
	public void setSkin(@Nullable UUID skin) {
		this.skin = skin;
	}

	/**
	 * Cancels the update (the real name and skin will be visible if <code>true</code>)
	 */
	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	/**
	 * @return <code>true</code> if the update is cancelled
	 */
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	private static HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
