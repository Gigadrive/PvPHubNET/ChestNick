package de.inventivegames.nickname.database;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.*;

@Entity()
@Table(name = "nicknamer_skin")
public class SkinEntry {

	@Id
	private int id;

	@Version
	private int version;

	@NotNull
	@Column(unique = true, nullable = false, updatable = false)
	private String owner;

	@NotNull
	@Column(unique = false, nullable = false, updatable = true)
	private String data;

	@NotNull
	private long lastUsage;

	public SkinEntry() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public long getLastUsage() {
		return lastUsage;
	}

	public void setLastUsage(long lastUsage) {
		this.lastUsage = lastUsage;
	}
}
