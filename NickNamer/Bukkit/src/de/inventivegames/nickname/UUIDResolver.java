/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.io.InputSupplier;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.Map.Entry;

/**
 * © Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 */
public class UUIDResolver extends Thread {

	protected static UUIDResolver instance;
	protected        boolean      active;

	private static int tries = 0;

	protected static final List<String>      queue   = new ArrayList<>();
	protected static final Map<String, UUID> uuidMap = new HashMap<>();

	public static void resolve(String owner) {
		queue.add(owner);
	}

	@Override
	public synchronized void start() {
		if (instance != null) { throw new IllegalStateException("UUIDResolver cannot be instantiated twice!"); }
		instance = this;
		this.active = true;
		super.start();
	}

	@Override
	public void run() {
		while (this.active) {
			try {
				this.runLoop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void runLoop() throws Exception {
		if (!queue.isEmpty()) {
			String current = queue.get(0);
			UUID id = null;
			try {
				id = this.resolveID(current);
			} catch (Exception e) {
				e.printStackTrace();
				tries++;
				if (tries < 3) {
					System.err.println("[NickNamer] Unable to load skin data. Retry in 120000ms");
					sleep(120000);
				} else {
					System.err.println("[NickNamer] Unable to load skin data. Removing from queue.");
					queue.remove(0);
				}
			}
			if (id != null) {
				tries = 0;
				UUID prevID = Bukkit.getOfflinePlayer(current).getUniqueId();

				List<UUID> owners = new ArrayList<>();
				for (Entry<UUID, UUID> entry : Nicks.skins.entrySet()) {
					if (entry.getValue().equals(prevID)) {
						owners.add(entry.getKey());
					}
				}
				for (UUID uid : owners) {// Swap the IDs
					Nicks.skins.remove(uid);
					Nicks.skins.put(uid, id);
					long l = SkinLoader.load(current);
					if (l < 0) {
						Nicks.updatePlayer(uid, false, true, true);
					}
				}
				queue.remove(0);
			}
		} else {
			sleep(10000);
		}
	}

	private UUID resolveID(final String name) throws Exception {
		System.out.println("[NickNamer] Resolving UUID for " + name);
		JSONObject json = (JSONObject) new JSONParser().parse(CharStreams.toString(new InputSupplier<InputStreamReader>() {
			@Override
			public InputStreamReader getInput() throws IOException {
				String url = String.format("https://api.mojang.com/users/profiles/minecraft/%s", name);
				return new InputStreamReader(new URL(url).openConnection().getInputStream(), Charsets.UTF_8);
			}
		}));
		if (json.containsKey("id")) {
			UUID id = UUID.fromString(((String) json.get("id")).replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));// Parse to default UUID format
			System.out.println("[NickNamer] " + name + "'s UUID is " + id);
			uuidMap.put(name, id);
			return id;
		} else { throw new IOException("Received invalid response from server: JSON does not contain ID"); }
	}

}
