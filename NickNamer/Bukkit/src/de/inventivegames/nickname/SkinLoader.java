/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.io.InputSupplier;
import de.inventivegames.nickname.database.SkinEntry;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * © Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 */
public class SkinLoader extends Thread {

	protected static SkinLoader instance;
	protected        boolean    active;

	public static int tries = 0;

	protected static final Map<UUID, JSONObject> skinStorage = new HashMap<>();
	protected static final List<String>          queue       = new ArrayList<>();

	protected static long lastLoad = System.currentTimeMillis() - 280000;

	public static long load(String owner) {
		if (instance == null) { throw new IllegalStateException("The SkinLoader has not been instantiated yet!"); }
		if (!skinStorage.containsKey(Bukkit.getOfflinePlayer(owner).getUniqueId()) && !queue.contains(owner)) {
			queue.add(owner);
			long time = NickNamer.API_TIMEOUT - nextLoad();
			if (time > 1) {
				System.out.println("[NickNamer] Skin data for " + owner + " will be downloaded in " + time + "ms.");
			}
			return time;
		}
		return -1L;
	}

	public static JSONObject getSkin(UUID owner) {
		if (instance == null) { throw new IllegalStateException("[NickNamer] The SkinLoader has not been instantiated yet!"); }
		return skinStorage.get(owner);
	}

	public static List<UUID> getLoadedSkins() {
		if (instance == null) { throw new IllegalStateException("[NickNamer] The SkinLoader has not been instantiated yet!"); }
		return new ArrayList<>(skinStorage.keySet());
	}

	@Override
	public synchronized void start() {
		if (instance == null) {
			instance = this;
			this.active = true;
			super.start();
		} else { throw new IllegalStateException("[NickNamer] Cannot instantiate the SkinLoader twice!"); }
	}

	@Override
	public void run() {
		while (this.active) {
			try {
				this.runLoad();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void runLoad() throws Exception {
		if (System.currentTimeMillis() - lastLoad > NickNamer.API_TIMEOUT) {// 5 minutes
			if (!queue.isEmpty()) {
				String current = queue.get(0);
				loadSkin(current);
				sleep(NickNamer.API_TIMEOUT + 10);
			} else {
				sleep(5000);
			}
		} else {
			sleep(5000);
		}
	}

	protected static void loadSkin(String owner) {
		UUID id = Bukkit.getOfflinePlayer(owner).getUniqueId();
		if (!Bukkit.getOnlineMode()) {
			if (UUIDResolver.uuidMap.containsKey(owner)) {
				id = UUIDResolver.uuidMap.get(owner);
			}
		}
		loadSkin(id, owner);
	}

	protected static void loadSkin(final UUID id, String owner) {
		try {
			lastLoad = System.currentTimeMillis();
			JSONObject json = (JSONObject) new JSONParser().parse(CharStreams.toString(new InputSupplier<InputStreamReader>() {
				@Override
				public InputStreamReader getInput() throws IOException {
					String url = String.format("https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false", id.toString().replace("-", ""));
					return new InputStreamReader(new URL(url).openConnection().getInputStream(), Charsets.UTF_8);
				}
			}));
			if (json != null) {
				skinStorage.put(id, json);
				System.out.println("[NickNamer] Successfully loaded skin data for " + owner);
				queue.remove(0);
				queue.remove(owner);

				for (UUID skin : Nicks.getPlayersWithSkin(id)) {
					Nicks.updatePlayer(skin, false, true, NickNamer.SELF_UPDATE);
					NickNamer.sendPluginMessage(Bukkit.getPlayer(skin), "data", id.toString(), json.toJSONString());
				}

				if (NickNamer.STORAGE_ENABLED) {
					SkinEntry entry = new SkinEntry();
					entry.setOwner(id.toString());
					entry.setData(json.toJSONString());
					entry.setLastUsage(System.currentTimeMillis());

					try {
						NickNamer.instance.getDatabase().save(entry);
						System.out.println("[NickNamer] Stored " + owner + "'s data in database");
					} catch (Exception e) {
						System.err.println("[NickNamer] Failed to store " + owner + "'s data: " + e.getMessage());
						e.printStackTrace();
					}
				}

			}
			tries = 0;
		} catch (Exception e) {
			System.err.println("[NickNamer] Exception while loading skin for " + owner + ": " + e.getMessage());
			e.printStackTrace();
			tries++;
			if (tries >= 3) {
				System.err.println("[NickNamer] Unable to load skin data. Removing from queue.");
				queue.remove(0);
			} else {
				try {
					sleep(NickNamer.API_TIMEOUT + 10);
				} catch (InterruptedException e1) {
				}
			}
		}
	}

	public static long nextLoad() {
		return System.currentTimeMillis() - lastLoad;
	}

}
