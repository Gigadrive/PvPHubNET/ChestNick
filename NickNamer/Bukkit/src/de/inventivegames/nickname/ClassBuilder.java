package de.inventivegames.nickname;

import org.bukkit.entity.Player;

import java.util.List;

public class ClassBuilder {


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object buildPlayerInfoPacket(int action, Object profile, int ping, int gamemodeOrdinal, String name) {
		try {
			Object packet = nmsPacketPlayOutPlayerInfo.newInstance();

			if (serverVersion < 180) {
				AccessUtil.setAccessible(nmsPacketPlayOutPlayerInfo.getDeclaredField("action")).set(packet, action);
				AccessUtil.setAccessible(nmsPacketPlayOutPlayerInfo.getDeclaredField("player")).set(packet, profile);
				AccessUtil.setAccessible(nmsPacketPlayOutPlayerInfo.getDeclaredField("gamemode")).set(packet, gamemodeOrdinal);
				AccessUtil.setAccessible(nmsPacketPlayOutPlayerInfo.getDeclaredField("ping")).set(packet, ping);
				AccessUtil.setAccessible(nmsPacketPlayOutPlayerInfo.getDeclaredField("username")).set(packet, name);
			} else {
				AccessUtil.setAccessible(nmsPacketPlayOutPlayerInfo.getDeclaredField("a")).set(packet, nmsEnumPlayerInfoAction.getEnumConstants()[action]);
				List list = (List) AccessUtil.setAccessible(nmsPacketPlayOutPlayerInfo.getDeclaredField("b")).get(packet);

				Object data;
				// if (NPCLib.getServerVersion() <= 181) {
				data = nmsPlayerInfoData.getConstructor(nmsPacketPlayOutPlayerInfo, getNMUtilClass("com.mojang.authlib.GameProfile"), int.class, nmsEnumGamemode, Reflection.getNMSClass("IChatBaseComponent")).newInstance(packet, profile, ping, nmsEnumGamemode.getEnumConstants()[gamemodeOrdinal], buildChatComponent(name));
				// } else {
				// data = nmsPlayerInfoData.getConstructor(getNMUtilClass("com.mojang.authlib.GameProfile"), int.class, nmsEnumGamemode,
				// Reflection.getNMSClass("IChatBaseComponent")).newInstance(profile, ping, nmsEnumGamemode.getEnumConstants()[gamemodeOrdinal], buildChatComponent(name));
				// }
				list.add(data);
			}
			return packet;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object buildChatComponent(String string) {
		Object comp = null;
		try {
			Object[] components = (Object[]) Reflection.getOBCClass("util.CraftChatMessage").getMethod("fromString", String.class).invoke(null, string);
			if (components.length > 0) {
				comp = components[0];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return comp;
	}

	public static Object getGameProfile(Player player) {
		try {
			return Reflection.getNMSClass("EntityHuman").getDeclaredMethod("getProfile").invoke(Reflection.getHandle(player));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Class<?> getNMUtilClass(String name) throws ClassNotFoundException {
		if (serverVersion < 180) { return Class.forName("net.minecraft.util." + name); } else { return Class.forName(name); }
	}

	private static boolean initialized = false;

	protected static int serverVersion = 170;

	protected static Class<?> nmsPacketPlayOutPlayerInfo;
	protected static Class<?> nmsPlayerInfoData;
	protected static Class<?> nmsEnumPlayerInfoAction;
	protected static Class<?> nmsEnumGamemode;

	static {
		if (!initialized) {
			if (Reflection.getVersion().contains("1_8")) {
				serverVersion = 180;
			}
			if (Reflection.getVersion().contains("1_8_R1")) {
				serverVersion = 181;
			}
			if (Reflection.getVersion().contains("1_8_R2")) {
				serverVersion = 182;
			}
			if (Reflection.getVersion().contains("1_8_R3")) {
				serverVersion = 183;
			}
			if (Reflection.getVersion().contains("1_7")) {
				serverVersion = 170;
			}

			try {
				nmsPacketPlayOutPlayerInfo = Reflection.getNMSClass("PacketPlayOutPlayerInfo");
				try {
					if (serverVersion > 170) {
						if (serverVersion <= 181) {
							nmsPlayerInfoData = Reflection.getNMSClassWithException("PlayerInfoData");
						} else {
							nmsPlayerInfoData = Reflection.getNMSClassWithException("PacketPlayOutPlayerInfo$PlayerInfoData");
						}
						if (serverVersion <= 181) {
							nmsEnumPlayerInfoAction = Reflection.getNMSClassWithException("EnumPlayerInfoAction");
						} else {
							nmsEnumPlayerInfoAction = Reflection.getNMSClassWithException("PacketPlayOutPlayerInfo$EnumPlayerInfoAction");
						}
					}
					if (serverVersion <= 181) {
						nmsEnumGamemode = Reflection.getNMSClassWithException("EnumGamemode");
					} else {
						nmsEnumGamemode = Reflection.getNMSClassWithException("WorldSettings$EnumGamemode");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				initialized = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
