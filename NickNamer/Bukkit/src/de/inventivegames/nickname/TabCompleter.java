package de.inventivegames.nickname;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;

import java.util.List;
import java.util.ListIterator;

public class TabCompleter implements Listener {

	@EventHandler
	public void onTabComplete(PlayerChatTabCompleteEvent e) {
		filterCompletions(e.getChatMessage(), (List<String>) e.getTabCompletions());
	}

	public static List<String> filterCompletions(String message, List<String> completions) {
		if (!NickNamer.TAB_OVERWRITE) {
			return completions;
		}

		ListIterator<String> iterator = completions.listIterator();

		if (!NickNamer.TAB_KEEP_ORIGINAL) {
			while (iterator.hasNext()) {
				String next = iterator.next();
				Player target = Bukkit.getPlayerExact(next);
				if (target != null) {
					if (Nicks.isNicked(target.getUniqueId())) {
						iterator.remove();
					}
				}
			}
		}

		List<String> nickCompletions = Nicks.getUsedNicks();
		nickCompletions = TabCompletionHelper.getPossibleCompletionsForGivenArgs(new String[] { message }, nickCompletions.toArray(new String[nickCompletions.size()]));
		completions.addAll(nickCompletions);

		return completions;
	}

}
