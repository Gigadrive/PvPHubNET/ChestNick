/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * © Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 */
public class CommandHandler implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!command.getName().equalsIgnoreCase("nickname") && !command.getName().equalsIgnoreCase("skin")) { return false; }

		if (args.length == 0) {
			if (sender.hasPermission("nick.command.name")) {
				sender.sendMessage("  ");
				sender.sendMessage(NickNamer.prefix + "§a/nick <nick> [player]");
				sender.sendMessage(NickNamer.prefix + "§a/nick random [player]");
				sender.sendMessage(NickNamer.prefix + "§a/nick check [player]");
				sender.sendMessage(NickNamer.prefix + "§a/nick clear/reset [player]");
			}
			if (sender.hasPermission("nick.command.skin")) {
				sender.sendMessage("  ");
				sender.sendMessage(NickNamer.prefix + "§a/skin <skin> [player]");
				sender.sendMessage(NickNamer.prefix + "§a/skin clear/reset [player]");
			}
			if (NickNamer.SINGLE_COMMAND && sender.hasPermission("nick.command.skin") && sender.hasPermission("nick.command.name")) {
				sender.sendMessage("  ");
				sender.sendMessage(NickNamer.prefix + "§a/nickskin <nick> <skin>");
			}
			return true;
		}

		//Single command
		if (NickNamer.SINGLE_COMMAND && command.getName().equalsIgnoreCase("nickskin")) {
			if (args.length != 2) {
				sender.sendMessage(NickNamer.prefix + "§cPlease provide the <nick> and <skin> arguments");
				return false;
			}
			String nick = args[0];
			String skin = args[1];

			if (sender instanceof Player) {
				((Player) sender).chat("/nickname " + nick);
				((Player) sender).chat("/skin " + skin);
			}
			return true;
		}

		// Nick name handling
		if (command.getName().equalsIgnoreCase("nickname")) {
			boolean otherTarget = false;
			Player target = null;
			if (args.length > 1) {
				target = Bukkit.getPlayer(args[1]);
				otherTarget = true;
			} else {
				if (!(sender instanceof Player)) {
					sender.sendMessage(NickNamer.prefix + "§cPlease provide the [player] argument");
					return false;
				}
				target = (Player) sender;
			}

			if ("check".equalsIgnoreCase(args[0])) {
				if (!sender.hasPermission("nick.command.name.check")) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_GENERAL);
					return false;
				}

				if (target != null && target.isOnline()) {
					// Check if the player is nicked
					String message = NickNamer.prefix + "§b" + target.getName();
					if (Nicks.isNicked(target.getUniqueId())) {
						message += "§a has the nick name §b" + Nicks.getNick(target.getUniqueId());
					} else {
						message += "§a has no nick name";
					}
					message += " §aand ";
					if (Nicks.hasSkin(target.getUniqueId())) {
						UUID skin = Nicks.getSkin(target.getUniqueId());
						OfflinePlayer op = Bukkit.getOfflinePlayer(skin);
						message += "§b" + (op != null ? op.getName() : skin) + "§a's skin.";
					} else {
						message += "their own skin.";
					}
					sender.sendMessage(message);

					return true;
				} else {
					// Check if the nick name is used
					String toCheck = args[1].replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
					String message = NickNamer.prefix + "§b" + toCheck;
					if (Nicks.isNickUsed(toCheck)) {
						message += "§a is used by ";
						String users = "§b";
						for (UUID id : Nicks.getPlayersWithNick(toCheck)) {
							Player user = Bukkit.getPlayer(id);
							if (user != null) {
								users += "§b" + user.getName() + "§r, ";
							}
						}
						users = users.substring(0, users.length() - 2);
						message += users;
					} else {
						message += "§a is not used";
					}
					sender.sendMessage(message);
					return true;
				}
			}

			if ("clear".equalsIgnoreCase(args[0]) || "reset".equalsIgnoreCase(args[0])) {
				if (!sender.hasPermission("nick.command.name.clear") && !sender.hasPermission("nick.command.name.reset")) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_GENERAL);
					return false;
				}
				if (target == null || !target.isOnline()) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PLAYER_NOT_ONLINE);
					return false;
				}
				Nicks.removeNick(target.getUniqueId());
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_CLEARED_NAME.replace("%player%", target.getName()));

				if (NickNamer.CHANGE_BOTH) {
					if (sender instanceof Player) {
						((Player) sender).chat("/skin clear " + target.getName());
					}
				}
				return true;
			}

			// Handle the actual nick changes
			if (!sender.hasPermission("nick.command.name")) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_GENERAL);
				return false;
			}
			if (otherTarget) {
				if (!sender.hasPermission("nick.other")) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_CHANGE_OTHER);
					return false;
				}
			}
			String nick = args[0].replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");

			if ("random".equalsIgnoreCase(nick)) {
				if (!sender.hasPermission("nick.command.name.random")) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_GENERAL);
					return false;
				}
				if (NickNamer.RANDOM_NAMES.isEmpty()) {
					sender.sendMessage(NickNamer.prefix + "§cNo random names configured!");
					return false;
				} else {
					nick = NickNamer.RANDOM_NAMES.get(new Random().nextInt(NickNamer.RANDOM_NAMES.size()));
				}
			}

			if (nick == null || nick.isEmpty()) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_NAME_INVALID);
				return false;
			}
			if (((NickNamer.MAX_LENGTH_IGNORE_COLOR && org.bukkit.ChatColor.stripColor(nick).length() > NickNamer.MAX_LENGTH) || (nick.length() > NickNamer.MAX_LENGTH)) || nick.length() > 16) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_NAME_LENGTH);
				return false;
			}
			if (nameTaken(nick, sender)) { return false; }

			if (target == null || !target.isOnline()) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PLAYER_NOT_ONLINE);
				return false;
			}

			if (!sender.hasPermission("nick.name." + nick)) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_NAME);
				return false;
			}

			try {
				Nicks.setNick(target.getUniqueId(), nick);
			} catch (Exception e) {
				sender.sendMessage(NickNamer.prefix + "§cException while changing name: §7" + e.getMessage());
				e.printStackTrace();
				return false;
			}
			if (NickNamer.CHANGE_BOTH) {
				if (sender instanceof Player) {
					((Player) sender).chat("/skin " + nick + " " + target.getName());
				}
			}
			sender.sendMessage(NickNamer.prefix + NickNamer.MSG_NAME_CHANGED.replace("%player%", target.getName()).replace("%name%", nick));
			return true;
		}

		// Skin handling
		if (command.getName().equalsIgnoreCase("skin")) {
			boolean otherTarget = false;
			Player target = null;
			if (args.length > 1) {
				target = Bukkit.getPlayer(args[1]);
				otherTarget = true;
			} else {
				if (!(sender instanceof Player)) {
					sender.sendMessage(NickNamer.prefix + "§cPlease provide the [player] argument");
					return false;
				}
				target = (Player) sender;
			}

			if ("clear".equalsIgnoreCase(args[0]) || "reset".equalsIgnoreCase(args[0])) {
				if (!sender.hasPermission("nick.command.skin.clear") && !sender.hasPermission("nick.command.skin.reset")) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_GENERAL);
					return false;
				}
				if (target == null || !target.isOnline()) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PLAYER_NOT_ONLINE);
					return false;
				}
				Nicks.removeSkin(target.getUniqueId());
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_CLEARED_SKIN.replace("%player%", target.getName()));

				return true;
			}

			// Handle the actual skin changes
			if (!sender.hasPermission("nick.command.skin")) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_GENERAL);
				return false;
			}
			if (otherTarget) {
				if (!sender.hasPermission("skin.other")) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_CHANGE_OTHER);
					return false;
				}
			}

			String skin = args[0];

			if ("random".equalsIgnoreCase(skin)) {
				if (!sender.hasPermission("nick.command.skin.random")) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_GENERAL);
					return false;
				}
				if (NickNamer.RANDOM_SKINS.isEmpty()) {
					sender.sendMessage(NickNamer.prefix + "§cNo random skins configured!");
					return false;
				} else {
					skin = NickNamer.RANDOM_SKINS.get(new Random().nextInt(NickNamer.RANDOM_SKINS.size()));
				}
			}

			if (skin.length() > 16) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_NAME_LENGTH);
				return false;
			}

			if (target == null || !target.isOnline()) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PLAYER_NOT_ONLINE);
				return false;
			}

			try {
				if (!sender.hasPermission("nick.skin." + skin)) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_PERM_SKIN);
					return false;
				}
				long delay = Nicks.setSkin(target.getUniqueId(), skin);
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_SKIN_CHANGED.replace("%player%", target.getName()).replace("%skin%", skin));
				if (delay > 0) {
					sender.sendMessage(NickNamer.prefix + NickNamer.MSG_SKIN_UPDATE.replace("%time%", new SimpleDateFormat("mm:ss").format(new Date(delay))));
				}
				return true;
			} catch (Exception e) {
				sender.sendMessage(NickNamer.prefix + "§cException while changing skin: §7" + e.getMessage());
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (command.getName().equalsIgnoreCase("nickname")) {
			if (args.length == 1) {
				if (sender.hasPermission("nick.command.name.random")) {
					list.add("random");
				}
				if (sender.hasPermission("nick.command.name.check")) {
					list.add("check");
				}
				if (sender.hasPermission("nick.command.name.clear")) {
					list.add("clear");
				} else if (sender.hasPermission("nick.command.name.reset")) {
					list.add("reset");
				}
			}
			if (args.length == 2) {
				list.addAll(TabCompletionHelper.getOnlinePlayerNames());
				if ("check".equalsIgnoreCase(args[0]) && sender.hasPermission("nick.command.name.check")) {
					for (String s : Nicks.getUsedNicks()) {
						list.add(s);
					}
				}
			}
		}
		if (command.getName().equalsIgnoreCase("skin")) {
			if (args.length == 1) {
				if (sender.hasPermission("nick.command.skin.clear")) {
					list.add("clear");
				} else if (sender.hasPermission("nick.command.skin.reset")) {
					list.add("reset");
				}
			}
			if (args.length == 2) {
				list.addAll(TabCompletionHelper.getOnlinePlayerNames());
			}
		}
		return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, list.toArray(new String[list.size()]));
	}

	public static boolean nameTaken(String nick, CommandSender sender) {
		if (NickNamer.DISABLE_EXISTING) {
			boolean exists = false;
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (p == sender) {
					continue;
				}
				if (nick.equals(p.getName())) {
					exists = true;
					break;
				}
			}
			if (!exists) {
				for (OfflinePlayer op : Bukkit.getOfflinePlayers()) {
					if (op == sender) {
						continue;
					}
					if (op != null && nick.equals(op.getName())) {
						exists = true;
						break;
					}
				}
			}
			if (exists) {
				sender.sendMessage(NickNamer.prefix + NickNamer.MSG_NAME_TAKEN);
				return true;
			}
		}
		return false;
	}

}
