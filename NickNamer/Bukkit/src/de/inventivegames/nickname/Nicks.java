/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.json.simple.JSONObject;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author inventivetalent
 */
public class Nicks {

	private static final   Map<UUID, String> nickedPlayers = new ConcurrentHashMap<>();
	private static final   Map<UUID, String> storedNames   = new ConcurrentHashMap<>();
	protected static final Map<UUID, UUID>   skins         = new ConcurrentHashMap<>();

	/**
	 * @param id {@link UUID} of the player
	 * @return <code>true</code> if the player has a nickname
	 */
	public static boolean isNicked(UUID id) {
		return nickedPlayers.containsKey(id);
	}

	/**
	 * @param nick nickname
	 * @return <code>true</code> if the nickname is used by a player
	 */
	public static boolean isNickUsed(String nick) {
		return nickedPlayers.containsValue(nick);
	}

	/**
	 * @param id {@link UUID} of the player
	 * @return nickname of the player
	 */
	public static String getNick(UUID id) {
		return nickedPlayers.get(id);
	}

	/**
	 * @param id   {@link UUID} of the player
	 * @param nick nickname to set
	 */
	public static void setNick(UUID id, String nick) {
		if (nick.length() > 16) { throw new IllegalArgumentException("Name is too long (" + nick.length() + " > 16)"); }
		if (isNicked(id)) {
			removeNick(id);
		}
		nickedPlayers.put(id, nick);

		Player p = Bukkit.getPlayer(id);
		if (p != null) {
			storedNames.put(id, p.getDisplayName());
			if (NickNamer.NICK_CHAT) {
				p.setDisplayName(nick);
			}
			if (NickNamer.NICK_TAB) {
				p.setPlayerListName(nick);
			}
			if (NickNamer.NICK_SCOREBOARD) {
				Scoreboard sb = p.getScoreboard();
				if (sb == null) {
					sb = Bukkit.getScoreboardManager().getMainScoreboard();
				}
				if (sb != null) {
					Team t = sb.getPlayerTeam(p);
					if (t != null) {
						t.removePlayer(p);
						t.addEntry(nick);
					}
				}
			}
		}

		NickNamer.sendPluginMessage(p, "name", nick);

		updatePlayer(id, true, false, NickNamer.SELF_UPDATE);
	}

	/**
	 * @param id {@link UUID} of the player
	 */
	public static void removeNick(UUID id) {
		String nick = nickedPlayers.remove(id);

		Player p = Bukkit.getPlayer(id);
		if (p != null) {
			if (NickNamer.NICK_CHAT) {
				p.setDisplayName(storedNames.get(id));
			}
			if (NickNamer.NICK_TAB) {
				p.setPlayerListName(storedNames.get(id));
			}
			if (NickNamer.NICK_SCOREBOARD) {
				Scoreboard sb = p.getScoreboard();
				if (sb == null) {
					sb = Bukkit.getScoreboardManager().getMainScoreboard();
				}
				if (sb != null) {
					Team t = null;
					for (Team tm : sb.getTeams()) {
						for (String s : tm.getEntries()) {
							if (s.equals(nick)) {
								t = tm;
								break;
							}
						}
					}
					if (t != null) {
						t.removeEntry(nick);
						t.addPlayer(p);
					}
				}
			}
		}
		storedNames.remove(id);

		NickNamer.sendPluginMessage(p, "name", "reset");

		updatePlayer(id, true, false, NickNamer.SELF_UPDATE);
	}

	/**
	 * @param nick nickname
	 * @return {@link List} of players with the nickname
	 */
	public static List<UUID> getPlayersWithNick(String nick) {
		List<UUID> list = new ArrayList<>();
		for (Entry<UUID, String> entry : nickedPlayers.entrySet()) {
			if (entry.getValue().equals(nick)) {
				list.add(entry.getKey());
			}
		}
		return Collections.unmodifiableList(list);
	}

	/**
	 * @return List of used nick names
	 */
	public static List<String> getUsedNicks() {
		return Collections.unmodifiableList(new ArrayList<>(nickedPlayers.values()));
	}

	/**
	 * @param id        {@link UUID} of the player
	 * @param skinOwner name of the skin owner
	 * @return time until the skin will be fully loaded
	 */
	public static long setSkin(UUID id, String skinOwner) {
		if (hasSkin(id)) {
			removeSkin(id);
		}

		@SuppressWarnings("deprecation")
		UUID skinID = Bukkit.getOfflinePlayer(skinOwner).getUniqueId();

		skins.put(id, skinID);

		NickNamer.sendPluginMessage(Bukkit.getPlayer(id), "skin", skinOwner);

		if (!Bukkit.getOnlineMode() && !NickNamer.BUNGEECORD) {
			UUIDResolver.resolve(skinOwner);
		} else {
			long l = SkinLoader.load(skinOwner);
			if (l == -1) {
				updatePlayer(id, false, true, NickNamer.SELF_UPDATE);
			}
			return l;
		}
		return -1L;
	}

	/**
	 * Load raw skin data to be used as custom skins
	 *
	 * @param key  (random) {@link UUID} as the skin owner
	 * @param data Skin data stored in a {@link JSONObject} (Must contain a valid 'properties' entry)
	 */
	public static boolean loadCustomSkin(UUID key, JSONObject data) {
		if (key == null || data == null) { throw new IllegalArgumentException("key and data cannot be null"); }
		if (!data.containsKey("properties")) { throw new IllegalArgumentException("JSONObject must contain 'properties' entry"); }
		SkinLoader.skinStorage.put(key, data);
		return true;
	}

	/**
	 * Set a previously loaded skin for a player
	 *
	 * @param id   {@link UUID} of the player
	 * @param skin {@link UUID}-key of the loaded skin
	 * @see #loadCustomSkin(UUID, JSONObject)
	 */
	public static void setCustomSkin(UUID id, UUID skin) {
		if (id == null || skin == null) { throw new IllegalArgumentException("id and skin cannot be null"); }
		if (!SkinLoader.skinStorage.containsKey(skin)) { throw new IllegalStateException("Specified skin has not been loaded yet"); }
		skins.put(id, skin);

		updatePlayer(id, false, true, NickNamer.SELF_UPDATE);
	}

	/**
	 * @param id {@link UUID} of the player
	 */
	public static void removeSkin(UUID id) {
		skins.remove(id);

		NickNamer.sendPluginMessage(Bukkit.getPlayer(id), "skin", "reset");

		updatePlayer(id, false, true, NickNamer.SELF_UPDATE);
	}

	/**
	 * @param id {@link UUID} of the player
	 * @return {@link UUID} of the skin owner
	 */
	public static UUID getSkin(UUID id) {
		if (hasSkin(id)) { return skins.get(id); }
		return id;
	}

	/**
	 * @param id {@link UUID} of the player
	 * @return <code>true</code> if the player has a skin
	 */
	public static boolean hasSkin(UUID id) {
		return skins.containsKey(id);
	}

	/**
	 * @param id {@link UUID} of the skin owner
	 * @return {@link List} of players with the skin
	 */
	public static List<UUID> getPlayersWithSkin(UUID id) {
		List<UUID> list = new ArrayList<>();
		for (Entry<UUID, UUID> entry : skins.entrySet()) {
			if (entry.getValue().equals(id)) {
				list.add(entry.getKey());
			}
		}
		return Collections.unmodifiableList(list);
	}

	/**
	 * Updates the player's name and skin to all online players
	 *
	 * @param id {@link UUID} of the player
	 */
	@Deprecated
	public static void updatePlayer(UUID id) {
		Player p = Bukkit.getPlayer(id);
		updatePlayer(p, true, true, NickNamer.SELF_UPDATE);
	}

	/**
	 * Updates the player's name and skin to all online players
	 *
	 * @param id   {@link UUID} of the player
	 * @param name update the name
	 * @param skin update the skin
	 * @param self update for the player themselves
	 */
	public static void updatePlayer(UUID id, boolean name, boolean skin, boolean self) {
		Player p = Bukkit.getPlayer(id);
		updatePlayer(p, name, skin, self);
	}

	/**
	 * Updates the player's name and skin to all online players
	 *
	 * @param p {@link Player}
	 */
	@Deprecated
	public static void updatePlayer(final Player p) {
		updatePlayer(p, true, true, NickNamer.SELF_UPDATE);
	}

	/**
	 * Updates the player's name and skin to all online players
	 *
	 * @param p    {@link Player}
	 * @param name update the name
	 * @param skin update the skin
	 * @param self update for the player themselves
	 */
	public static void updatePlayer(final Player p, boolean name, boolean skin, boolean self) {
		if (p == null) { return; }

		if (self) {
			updateSelf(p);
		}

		if (!skin && !name) { return; }

		Bukkit.getScheduler().scheduleSyncDelayedTask(NickNamer.instance, new Runnable() {

			@Override
			public void run() {
				List<Player> canSee = new ArrayList<>();
				for (Player p1 : Bukkit.getOnlinePlayers()) {
					if (p1.canSee(p)) {
						canSee.add(p1);
						p1.hidePlayer(p);
					}
				}
				for (Player p1 : canSee) {
					p1.showPlayer(p);
				}
			}
		});
	}

	protected static void updateSelf(final Player p) {
		if (p == null || !p.isOnline()) { return; }
		Object profile = ClassBuilder.getGameProfile(p);

		try {
			final Object removePlayer = ClassBuilder.buildPlayerInfoPacket(4, profile, 0, p.getGameMode().ordinal(), p.getName());
			final Object addPlayer = ClassBuilder.buildPlayerInfoPacket(0, profile, 0, p.getGameMode().ordinal(), p.getName());
			Object difficulty = enumDifficulty.getDeclaredMethod("getById", int.class).invoke(null, p.getWorld().getDifficulty().getValue());
			Object type = ((Object[]) worldType.getDeclaredField("types").get(null))[0];
			Object gamemode = enumGamemode.getDeclaredMethod("getById", int.class).invoke(null, p.getGameMode().getValue());
			final Object respawnPlayer = Reflection.getNMSClass("PacketPlayOutRespawn").getConstructor(int.class, enumDifficulty, worldType, enumGamemode).newInstance(0, difficulty, type, gamemode);

			Reflection.sendPacket(p, removePlayer);

			Bukkit.getScheduler().runTask(NickNamer.instance, new Runnable() {
				@Override
				public void run() {
					boolean flying = p.isFlying();
					Location location = p.getLocation();

					Reflection.sendPacket(p, respawnPlayer);

					p.setFlying(flying);
					p.teleport(location);
					p.updateInventory();

					Reflection.sendPacket(p, addPlayer);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected static boolean initialized = false;
	protected static Class enumDifficulty;
	protected static Class worldType;
	protected static Class enumGamemode;

	static {
		if (!initialized) {
			enumDifficulty = Reflection.getNMSClass("EnumDifficulty");
			worldType = Reflection.getNMSClass("WorldType");
			if (ClassBuilder.serverVersion < 182) {
				enumGamemode = Reflection.getNMSClass("EnumGamemode");
			} else {
				enumGamemode = Reflection.getNMSClass("WorldSettings$EnumGamemode");
			}

			initialized = true;
		}
	}
}
