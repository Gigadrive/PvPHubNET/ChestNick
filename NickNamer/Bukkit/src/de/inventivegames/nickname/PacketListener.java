/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import de.inventivegames.nickname.event.NickNamerUpdateEvent;
import de.inventivegames.packetlistener.handler.PacketHandler;
import de.inventivegames.packetlistener.handler.PacketOptions;
import de.inventivegames.packetlistener.handler.ReceivedPacket;
import de.inventivegames.packetlistener.handler.SentPacket;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.lang.reflect.Field;
import java.util.*;

/**
 * © Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 */
public class PacketListener {

	private static int version = 170;

	private static Class<?> IChatBaseComponent;
	private static Class<?> ChatSerializer;
	private static Class<?> classGameProfile;
	private static Class<?> classProperty;
	private static Class<?> classMultiMap;
	private static Class<?> classPropertyMap;

	static {
		try {
			if (Reflection.getVersion().contains("1_7")) {
				version = 170;
			}
			if (Reflection.getVersion().contains("1_8")) {
				version = 180;
			}
			if (Reflection.getVersion().contains("1_8_R1")) {
				version = 181;
			}
			if (Reflection.getVersion().contains("1_8_R2")) {
				version = 182;
			}

			IChatBaseComponent = Reflection.getNMSClass("IChatBaseComponent");
			if (version < 180) {
				ChatSerializer = Reflection.getNMSClass("ChatSerializer");
			} else {
				ChatSerializer = Reflection.getNMSClass("IChatBaseComponent$ChatSerializer");
			}

			if (version < 180) {
				classGameProfile = Class.forName("net.minecraft.util.com.mojang.authlib.GameProfile");
				classProperty = Class.forName("net.minecraft.util.com.mojang.authlib.properties.Property");
				classPropertyMap = Class.forName("net.minecraft.util.com.mojang.authlib.properties.PropertyMap");
				classMultiMap = Class.forName("net.minecraft.util.com.google.common.collect.Multimap");
			} else {
				classGameProfile = Class.forName("com.mojang.authlib.GameProfile");
				classProperty = Class.forName("com.mojang.authlib.properties.Property");
				classPropertyMap = Class.forName("com.mojang.authlib.properties.PropertyMap");
				classMultiMap = Class.forName("com.google.common.collect.Multimap");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static PacketHandler handler;
	protected Map<UUID, String> pendingCompletions = new HashMap<>();

	public PacketListener(Plugin pl) {
		PacketHandler.addHandler(handler = new PacketHandler(pl) {

			@Override
			@PacketOptions(forcePlayer = true)
			public void onSend(SentPacket packet) {
				if (NickNamer.NICK_NAME && packet.getPacketName().equals("PacketPlayOutNamedEntitySpawn") && version < 180) {
					if (packet.hasPlayer()) {
						try {
							Object profile = packet.getPacketValue("b");
							if (profile != null) {
								packet.setPacketValue("b", disguiseProfile(packet.getPlayer(), profile));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				if (NickNamer.NICK_NAME && packet.getPacketName().equals("PacketPlayOutPlayerInfo")) {
					if (packet.hasPlayer()) {
						if (version < 180 && (int) packet.getPacketValue("action") == 4) {
							return;// Cancel here if the player is currently being removed
						}
						try {
							Object profile = packet.getPacketValue(version < 180 ? "player" : "b");
							if (version < 180) {
								if (profile != null) {
									profile = disguiseProfile(packet.getPlayer(), profile);
									packet.setPacketValue("player", profile);
								}
								if (profile != null) {
									packet.setPacketValue("username", AccessUtil.setAccessible(profile.getClass().getDeclaredField("name")).get(profile));
								}
							} else {// PlayerInfoData handling
								List list = new ArrayList<>((List) profile);
								for (Object obj : list) {
									Field profileField = AccessUtil.setAccessible(obj.getClass().getDeclaredField("d"));
									profile = disguiseProfile(packet.getPlayer(), profileField.get(obj));
									profileField.set(obj, profile);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				if (NickNamer.TAB_OVERWRITE && packet.getPacketName().equals("PacketPlayOutTabComplete")) {
					if (pendingCompletions.containsKey(packet.getPlayer().getUniqueId())) {
						String[] array = (String[]) packet.getPacketValue("a");
						List<String> list = new ArrayList<>(Arrays.asList(array));

						String message = pendingCompletions.get(packet.getPlayer().getUniqueId());
						int index = message.lastIndexOf(' ');
						if (index != -1) {
							message = message.substring(index, message.length()).substring(1);
						}
						list = TabCompleter.filterCompletions(message, list);
						array = list.toArray(new String[list.size()]);

						packet.setPacketValue("a", array);

						pendingCompletions.remove(packet.getPlayer().getUniqueId());
					}
				}
				if (NickNamer.NICK_VANILLA && packet.getPacketName().equals("PacketPlayOutChat")) {
					Object a = packet.getPacketValue("a");
					try {
						String raw = (String) ChatSerializer.getDeclaredMethod("a", IChatBaseComponent).invoke(null, a);
						for (Player player : Bukkit.getOnlinePlayers()) {
							if (Nicks.isNicked(player.getUniqueId())) {
								String nick = Nicks.getNick(player.getUniqueId());
								if (raw.toLowerCase().contains(player.getName().toLowerCase())) {
									raw = raw.replaceAll("(?i)" + player.getName().toLowerCase(), nick);
								}
							}
						}
						Object serialized = ChatSerializer.getDeclaredMethod("a", String.class).invoke(null, raw);
						packet.setPacketValue("a", serialized);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			@PacketOptions(forcePlayer = true)
			public void onReceive(ReceivedPacket packet) {
				if (NickNamer.TAB_OVERWRITE && packet.getPacketName().equals("PacketPlayInTabComplete")) {
					String message = (String) packet.getPacketValue("a");
					if (message != null && message.startsWith("/")) {
						pendingCompletions.put(packet.getPlayer().getUniqueId(), message);
					}
				}
				if (NickNamer.NICK_COMMANDS && packet.getPacketName().equals("PacketPlayInChat")) {
					String a = (String) packet.getPacketValue("a");
					if(!a.startsWith("/"))return;
					for (Player player : Bukkit.getOnlinePlayers()) {
						if (Nicks.isNicked(player.getUniqueId())) {
							String nick = Nicks.getNick(player.getUniqueId());
							if (a.toLowerCase().contains(nick.toLowerCase())) {
								a = a.replaceAll("(?i)" + nick.toLowerCase(), player.getName());
							}
						}
					}
					packet.setPacketValue("a", a);
				}
			}
		});
	}

	private static Object disguiseProfile(final Player observer, final Object profile) throws Exception {
		Field idField = AccessUtil.setAccessible(profile.getClass().getDeclaredField("id"));
		Field nameField = AccessUtil.setAccessible(profile.getClass().getDeclaredField("name"));
		Field propertyMapField = AccessUtil.setAccessible(profile.getClass().getDeclaredField("properties"));

		final UUID id = (UUID) idField.get(profile);
		final String name = (String) nameField.get(profile);
		final Object propertyMap = propertyMapField.get(profile);

		if (!Nicks.isNicked(id) && !Nicks.hasSkin(id)) {
			return profile;// return the profile as it is if nothing has to be changed
		}

		String nick = name;
		UUID skin = id;

		if (Nicks.isNicked(id)) {
			nick = Nicks.getNick(id);
		}
		if (Nicks.hasSkin(id)) {
			skin = Nicks.getSkin(id);
		}

		//Call the update event
		NickNamerUpdateEvent event = new NickNamerUpdateEvent(Bukkit.getPlayer(id), observer, nick, skin);
		Bukkit.getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			return profile;//Don't change anything if the event is cancelled
		}

		//Update the variables (if they aren't null)
		if (event.getNick() != null) {
			nick = event.getNick();
		}
		if (event.getSkin() != null) {
			skin = event.getSkin();
		}

		final Object profileClone = classGameProfile.getConstructor(UUID.class, String.class).newInstance(id, name);// Create a clone of the profile since the server's PlayerList will use the original profiles
		Object propertyMapClone = classPropertyMap.newInstance();
		classPropertyMap.getSuperclass().getMethod("putAll", classMultiMap).invoke(propertyMapClone, propertyMap);

		if (Nicks.hasSkin(id)) {
			JSONObject skinData = SkinLoader.getSkin(skin);
			if (skinData != null && skinData.containsKey("properties")) {
				skinData = (JSONObject) ((JSONArray) skinData.get("properties")).get(0);

				if (skinData != null) {
					classPropertyMap.getMethod("clear").invoke(propertyMapClone);
					classPropertyMap.getSuperclass().getMethod("put", Object.class, Object.class).invoke(propertyMapClone, "textures", classProperty.getConstructor(String.class, String.class, String.class).newInstance("textures", skinData.get("value"), skinData.get("signature")));
				}
			} else {
				if (NickNamer.LOADING_SKIN) {
					// Clear the skin data to display the Steve/Alex skin
					classPropertyMap.getMethod("clear").invoke(propertyMapClone);
				}
			}
		}

		propertyMapField.set(profileClone, propertyMapClone);

		if (Nicks.isNicked(id)) {
			nameField.set(profileClone, nick);
		}
		return profileClone;
	}

	public void disable() {
		PacketHandler.removeHandler(handler);
	}

}
